Regression Week 4: Ridge Regression Assignment 1

In this assignment, we will run ridge regression multiple times with different L2 penalties to see which one produces the best fit. We will revisit the example of polynomial regression as a means to see the effect of L2 regularization. In particular, we will:

Use a pre-built implementation of regression to run polynomial regression
Use matplotlib to visualize polynomial regressions
Use a pre-built implementation of regression to run polynomial regression, this time with L2 penalty
Use matplotlib to visualize polynomial regressions under L2 regularization
Choose best L2 penalty using cross-validation.
Assess the final fit using test data.


Regression Week 4: Ridge Regression Assignment 2

In this assignment, you will implement ridge regression via gradient descent. You will:

Convert an SFrame into a Numpy array (if applicable)
Write a Numpy function to compute the derivative of the regression weights with respect to a single feature
Write gradient descent function to compute the regression weights given an initial weight vector, step size, tolerance, and L2 penalty
