Regression Week 1: Simple Linear Regression Assignment

Predicting House Prices (One feature)

In this notebook we will use data on house sales in King County, where Seattle is located, to predict house prices using simple (one feature) linear regression. You will:

Use SArray and SFrame functions to compute important summary statistics
Write a function to compute the Simple Linear Regression weights using the closed form solution
Write a function to make predictions of the output given the input feature
Turn the regression around to predict the input/feature given the output
Compare two different models for predicting house prices
