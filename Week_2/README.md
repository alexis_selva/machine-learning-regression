Regression Week 2: Multiple Linear Regression Assignment 1

Predicting House Prices (Multiple Variables)

In this notebook you will use data on house sales in King County to predict prices using multiple regression. The first assignment will be about exploring multiple regression in particular exploring the impact of adding features to a regression and measuring error. In the second assignment you will implement a gradient descent algorithm. In this assignment you will:

Use SFrames to do some feature engineering
Use built-in GraphLab Create (or otherwise) functions to compute the regression weights (coefficients)
Given the regression weights, predictors and outcome write a function to compute the Residual Sum of Squares
Look at coefficients and interpret their meanings
Evaluate multiple models via RSS


Regression Week 2: Multiple Linear Regression Quiz 2

Estimating Multiple Regression Coefficients (Gradient Descent)

In the first notebook we explored multiple regression using GraphLab Create. Now we will use SFrames along with numpy to solve for the regression weights with gradient descent.

In this notebook we will cover estimating multiple regression weights via gradient descent. You will:

Add a constant column of 1's to a SFrame (or otherwise) to account for the intercept
Convert an SFrame into a numpy array
Write a predict_output() function using numpy
Write a numpy function to compute the derivative of the regression weights with respect to a single feature
Write gradient descent function to compute the regression weights given an initial weight vector, step size and tolerance.
Use the gradient descent function to estimate regression weights for multiple features
