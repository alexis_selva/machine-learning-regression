Regression Week 3: Polynomial Regression Quiz

In this notebook you will compare different regression models in order to assess which model fits best. We will be using polynomial regression as a means to examine this topic. In particular you will:

Write a function to take an an array and a degree and return an data frame where each column is the array to a polynomial value up to the total degree.
Use a plotting tool (e.g. matplotlib) to visualize polynomial regressions
Use a plotting tool (e.g. matplotlib) to visualize the same polynomial degree on different subsets of the data
Use a validation set to select a polynomial degree
Assess the final fit using test data
