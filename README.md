These are the programming assignments relative to Machine Learning - Regression (2nd part):

* Assignment 1: predicting House Prices (one feature)
* Assignment 2: Predicting House Prices (multiple Variables)
* Assignment 3: comparing different regression models in order to assess which model fits best
* Assignment 4: running ridge regression multiple times with different L2 penalties to see which one produces the best fit
* Assignment 5: using LASSO to select features, building on a pre-implemented solver for LASSO
* Assignment 6: implementing k-nearest neighbors regression

For more information, I invite you to have a look at https://www.coursera.org/learn/ml-regression