Predicting house prices using k-nearest neighbors regression

In this notebook, you will implement k-nearest neighbors regression. You will:

Find the k-nearest neighbors of a given query input
Predict the output for the query input using the k-nearest neighbors
Choose the best value of k using a validation set
